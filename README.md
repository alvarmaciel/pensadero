# El pensadero

Sitio desarrollado a partir del trabajo de [Jethro](https://blog.jethro.dev/posts/automatic_publishing/).
En este sitio se suben las notas públicas que voy tomando.

[![Netlify Status](https://api.netlify.com/api/v1/badges/38103b9f-2944-4ba9-a32b-3574ce3b631c/deploy-status)](https://app.netlify.com/sites/frosty-curran-f9b543/deploys)
