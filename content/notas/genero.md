+++
title = "genero"
author = ["Alvar Maciel"]
lastmod = 2020-05-14T12:31:25-03:00
slug = "genero"
draft = false
+++

## Backlinks {#backlinks}

-   [Rethinking Artificial Intelligence through Feminism | CCCB LAB]({{< relref "rethinking_artificial_intelligence_through_feminism_cccb_lab" >}})
