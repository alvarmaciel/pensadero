+++
title = "ciudadania digital y seguridad"
author = ["Alvar Maciel"]
lastmod = 2020-05-20T14:40:49-03:00
slug = "ciudadania_digital_y_seguridad"
draft = false
+++

tags
: [ciudadania digital]({{< relref "ciudadania_digital" >}}), [sadosky]({{< relref "sadosky" >}})


Source : <http://program.ar/manual-segundo-ciclo-secundaria/>


## Información sensible y amenazas en la red {#información-sensible-y-amenazas-en-la-red}

-   Todo el contenido se encamina a el riesgo de que las publicaciones sean vistas por "otros físicos" y lo que ahora nos preocupa son los otros "algorítmics" que perfilan nuestras identidades alimentando grandes bases de datos que se usan para sistemas predictivos.
