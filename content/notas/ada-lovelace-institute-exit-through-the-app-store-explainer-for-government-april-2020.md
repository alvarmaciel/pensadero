+++
title = "Ada-Lovelace-Institute-Exit-through-the-App-Store-Explainer-for-Government-April-2020.pdf"
author = ["Alvar Maciel"]
lastmod = 2020-05-14T12:49:05-03:00
slug = "Ada lovelace institute exit through the app store expaliner for government april 2020"
draft = false
+++

tags
: [ciudadania digital]({{< relref "ciudadania_digital" >}})


## Resumen {#resumen}


### Digital contact tracing applications {#digital-contact-tracing-applications}

-   Basados en la evidencia actual, significativas limitaciones técnicas y profundos riesgos sociales en la implementación de estas tecnología

<!--list-separator-->

-  Limitaciones técnicas:

    -   Imprecisiones en la detección del 'contacto'
    -   Impresiciones en la detección de la distancia entre la gente.
    -   Vulnerabilidades de fraude o abuso

<!--list-separator-->

-  Consideraciones sociales

    -   Potencial exclusoon de grupos vulnerables
    -   Implicaciones sociales y financieras;
    -   Criminalidad y estafa

<!--list-separator-->

-  Recomendación

    Para superar estas limitaciones y riesgos, se recomienda a los gobiernos gesnerar legislaciones y espablecer un Equipo Multidiciplinario de Asesores de Tecnologías en Emergencias (EMATE) Que tengan la función de defensorías en el despliegue de estas tecnologías.


### Symptom tracking applications {#symptom-tracking-applications}

Estos sistemas sufren de limitaciones que incluyen la baja calidad de los datos obtenidos a partir de el auto registro de los sintomas. Desequilibrios en la representación de los datos recolectados y riesgo de reportes falsos

<!--list-separator-->

-  Risgos sobre la salud pública

    -   La limitación de los datos puede confundir la evidencia de base
    -   Las desigualdades en saludo pueden incrementarse

<!--list-separator-->

-  Riesgos sobre los datos

    -   Los datos pueden compartirse más ampliamente de lo que se pretendía originalmente y las plataformas pueden reutilizarse;
    -   Las bases de datos de los sintomas centralizan gran cantidad de datos perosnales
    -   La organización que reúne los datos podrían quedarselos y utilizarlos luego de que la crísis termine para otro propósitos.

<!--list-separator-->

-  Recomendación

    Mitigar estos riesgos requiere que los gobiernos avancesn en legislaciones que limiten tanto el propósito para el cúal se procesan los datos de  las applicaciones de seguimientos de Síntomas, y el período de tiempo en el cual el procesamiento de estos datos se puede hacer antes de borrarlos.


### Digital immunity certificates {#digital-immunity-certificates}

Tdoavía no hay evidencia científica robusta para testear la inmunidad. Por lo tanto, no existe un fundamento legítimo para establecer un régimen integral de certificación de inmunidad en este momento.

Un análisis rápido de la evidencia permite estrablecere 4 cuestiones que deben ser resueltas en orden para orientar la formulación de plíticas sobre la certificación de inmunidad.

1.  ¿Son los conocimientos científicso sobre inmunidad lo suficientemente sólidos como para garantizar una política que se enfoque en  en la certificación de inmunidad?
2.  ¿Cómo se realizarán las pruebas de inmunidad?
3.  ¿Cómo se certificaran las pruebas de inmuinidad?
4.  ¿Cómo se integraría la certificación de inmunidad en los procesos y políticas?

Establecer un regimenr de certificación de inmunidad tendría profundas implicaciones sociales, incluyendo:

-   La violación de los derechos individuales, particularmente la privacidad;
-   Estigmatización y discriminación;
-   Establecimiento de dos niveles sociales;
-   Alto riesgo de fraude y abuso
-   Creación de incentivos perversos.
