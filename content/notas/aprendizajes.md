+++
title = "aprendizajes"
author = ["Alvar Maciel"]
lastmod = 2020-06-10T13:19:11-03:00
slug = "aprendizajes"
draft = false
+++

## Backlinks {#backlinks}

- [Rethinking Artificial Intelligence through Feminism | CCCB LAB]({{< relref "rethinking_artificial_intelligence_through_feminism_cccb_lab" >}})
- [Vigilar y educar – Culturalmente Digitales]({{< relref "vigilar_y_educar_culturalmente_digitales" >}})
- [Security Education Companion]({{< relref "security_education_companion" >}})
- [Teaching and Learning with Jupyter]({{< relref "teaching_and_learning_with_jupyter" >}})
- [Developing a Theory of Gamified Learning: Linking Serious Games and G…]({{< relref "developing_a_theory_of_gamified_learning_linking_serious_games_and_g" >}})
- [Mysterious Drugs that (Re)train Your Mind - my third brain]({{< relref "mysterious_drugs_that_re_train_your_mind_my_third_brain" >}})
- [teoria del actor red en educacion]({{< relref "teoria_del_actor_red_en_educacion" >}})
