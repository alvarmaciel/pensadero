+++
title = "teoria del actor red en educacion"
author = ["Alvar Maciel"]
lastmod = 2020-06-10T14:05:10-03:00
slug = "teoria_del_actor_red_en_educacion"
draft = false
+++

tags
: [aprendizajes]({{< relref "aprendizajes" >}}), [libros]({{< relref "libros" >}})

## Notas del libro {#notas-del-libro}

### ni de los estudios en la formación de los docentes {#ni-de-los-estudios-en-la-formación-de-los-docentes}

### Se estatá pensando en esto en ML {#se-estatá-pensando-en-esto-en-ml}

eNos invita a evitar los sesgos a priori que luego actúan como cimientos sobre los cuales se construyen
otros conocimientos. Los sesgos, como la distinción que existe entre lo social y lo natural, entre lo material y lo
cultural, lo humano y lo no humano, y entre lo técnico y lo social, se consideran efectos más que supuestos
fundantes.

### En TAR, por lo tanto, la sociedad y lo social no son vistos como un objeto de investigación preexistente, sino como emergentes del ejercicio de diversas formas de asociación, como efectos de red. {#en-tar-por-lo-tanto-la-sociedad-y-lo-social-no-son-vistos-como-un-objeto-de-investigación-preexistente-sino-como-emergentes-del-ejercicio-de-diversas-formas-de-asociación-como-efectos-de-red-dot}
