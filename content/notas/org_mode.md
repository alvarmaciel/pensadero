+++
title = "org-mode"
author = ["Alvar Maciel"]
lastmod = 2020-06-10T15:21:33-03:00
slug = "org_mode"
draft = false
+++

## Backlinks {#backlinks}

[[[{{< relref "an_orgmode_note_workflow_rohit_goswami_reflections" >}}]({{< relref "an_orgmode_note_workflow_rohit_goswami_reflections" >}})][an orgmode note workflow : rohit goswami — reflections]]
