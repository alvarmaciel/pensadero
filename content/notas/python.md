+++
title = "Python"
author = ["Alvar Maciel"]
lastmod = 2020-05-28T13:41:55-03:00
slug = "Python"
draft = false
+++

## Backlinks {#backlinks}

- [Teaching and Learning with Jupyter]({{< relref "teaching_and_learning_with_jupyter" >}})
- [pull request con issue]({{< relref "pull_request_con_issue" >}})
- [Guía para contribuir en la traducción — Documentación de Python en Español -- 3.8.3rc1]({{< relref "guia_para_contribuir_en_la_traduccion_documentacion_de_python_en_espanol_3_8_3rc1" >}})
