+++
title = "Developing a Theory of Gamified Learning: Linking Serious Games and G…"
author = ["Alvar Maciel"]
lastmod = 2020-05-28T13:37:08-03:00
draft = false
+++

tags
: [aprendizajes]({{< relref "aprendizajes" >}}), [juegos]({{< relref "juegos" >}})

source: <https://www.slideshare.net/eraser/developing-a-theory-of-gamified-learning-linking-serious-games-and-gamification-of-learning-richard-n-landers?from%5Faction=save>

PDF : [Developing a theory of gamified lerning](/ox-hugo/developping-200526060632.pdf)
