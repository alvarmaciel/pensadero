+++
title = "ciudadania digital"
author = ["Alvar Maciel"]
lastmod = 2020-05-20T15:05:19-03:00
slug = "ciudadania_digital"
draft = false
+++

## Backlinks {#backlinks}

- [Ada-Lovelace-Institute-Exit-through-the-App-Store-Explainer-for-Government-April-2020.pdf]({{< relref "ada-lovelace-institute-exit-through-the-app-store-explainer-for-government-april-2020" >}})
- [Security Education Companion]({{< relref "security_education_companion" >}})
- [Tactical Tech – "Technology is Stupid": How to Choose Tech for Remote Working]({{< relref "tactical_tech_technology_is_stupid_how_to_choose_tech_for_remote_working" >}})
- [ciudadania digital y seguridad]({{< relref "ciudadania_digital_y_seguridad" >}})
