+++
title = "pull request con issue"
author = ["Alvar Maciel"]
lastmod = 2020-05-28T13:58:23-03:00
slug = "pull_request_con_issue"
draft = false
+++

tags
: [Python]({{< relref "python" >}}), [git]({{< relref "git" >}})

## protip: {#protip}

—para PR futuros— Al abrir el PR, en la descripción puedes poner closes #99 donde 99 es un ejemplo, reemplazalo por el issue correspondiente, cosa que cuando se acepta el PR, el issue se cierra automáticamente 😉
