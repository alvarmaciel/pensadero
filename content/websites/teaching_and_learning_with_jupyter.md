+++
title = "Teaching and Learning with Jupyter"
author = ["Alvar Maciel"]
lastmod = 2020-05-28T13:58:33-03:00
slug = "teaching and learning with jupiter"
draft = false
+++

tags
: [aprendizajes]({{< relref "aprendizajes" >}}), [Python]({{< relref "python" >}})

## Notas del texto {#notas-del-texto}

- las notebooks son documentos que contienen texto narrativo con imágenes, matemática, codigo y la ejecución del código.

- Por qué usar Jupyter Notebooks

  - Diseñamos experiencias y ambientes de aprendizajes.

  - Jupyter es una conjunto de standars, una comunidad y una serie de softwares.

  - Basicamente Jupyter netbook nos permite tener en un solo documento datos, código y prosa para contar una historia, coumputacional intercativa.

  - Las notebooks pueden comninar expicaciones tradicionales con la interactividad de una aplicación.

  - Las Jupyter notebooks permiten estableces conversaciones entre los estudiantes y los problemas, linkearlos a recursos audiovisiales incluso permitiendo que los estudiantes remezclen estos recursos. Y todo tan solo usando un browser

  - Una plataforma para crear narrativas computacionales

  - Jupyter nos perimte estrablecer una conversación entre los estudianres y los datos.

  -

## Ideas {#ideas}

- Como sería pensar lel análisis de un problema matemático desde 0 con el uso de jupyter. Que tendría más valor pedagógico.
- un primer paso sería adaptar lo que hay para después o durante, empezar a pensar nuevas propuestas
