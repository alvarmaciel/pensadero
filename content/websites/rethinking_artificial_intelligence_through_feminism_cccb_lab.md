+++
title = "Rethinking Artificial Intelligence through Feminism | CCCB LAB"
author = ["Alvar Maciel"]
lastmod = 2020-05-14T12:29:34-03:00
draft = false
+++

tags
: [aprendizajes]({{< relref "aprendizajes" >}}), [genero]({{< relref "genero" >}})
