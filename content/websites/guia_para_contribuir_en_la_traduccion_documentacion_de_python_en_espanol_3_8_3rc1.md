+++
title = "Guía para contribuir en la traducción — Documentación de Python en Español – 3.8.3rc1"
author = ["Alvar Maciel"]
lastmod = 2020-05-28T14:01:24-03:00
draft = false
+++

tags
: [Python]({{< relref "python" >}})

source : <https://python-docs-es.readthedocs.io/es/3.8/CONTRIBUTING.html>
